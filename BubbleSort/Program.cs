﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BubbleSort
{
    class Program
    {
        static void BubbleSort(int[] array)
        {
            for (int i = 1; i < array.Length-1; i++)
            {
                for (int j = i+1; j < array.Length; j++)
                {
                    if (array[j] > array[j + 1])
                    {
                        int temp = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = temp;
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            int [] arrayForSort = new int[]{ 2, 3, 5, 6, 56, 45, 34, 78, 89, 90, 45, 34};
            BubbleSort(arrayForSort);
            Console.ReadKey();
        }
    }
}
