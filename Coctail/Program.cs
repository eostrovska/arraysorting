﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coctail
{
    class Program
    {
        static string Cocktail(string fruit, string ice, string alcohol)
        {
            string cocktail = fruit + ice + alcohol;
            Console.WriteLine(cocktail);
            Console.WriteLine("This is alcohol cocktail");
            return cocktail;
        }

        static string Cocktail(ref int price, string milkProduct, string ice, string fruit)
        {
            int newPrice = price * 2;
            string cocktail = milkProduct + ice + fruit;
            Console.WriteLine("This is milk cocktail");
            Console.WriteLine("Cocktail price is {0}", price);
            Console.WriteLine();
            return cocktail;
        }

        static int Cocktail(out int price, string milkProduct, string ice, string fruit, string solomka)
        {
            price = 2;
            string cocktail = milkProduct + ice + fruit + solomka;
            Console.WriteLine("This is child milk cocktail");
            Console.WriteLine("Cocktail price is {0}", price);
            Console.WriteLine();
            return price * 2;
        }

        static void Main(string[] args)
        {
            int price = 45;
            Cocktail("lime", "ice", "brendy");
            Cocktail(ref price, "milk", "ice", "banana");
            Cocktail(out price, "milk", "ice", "peach", "solomka");
            Cocktail("lime", alcohol: "visky", "ice");
            Console.ReadKey();
        }
    }
}
