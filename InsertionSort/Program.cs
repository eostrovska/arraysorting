﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsertionSort
{
    class Program
    {
        public void InsertionSort(int[] array)
        {
            for (int i = 1; i < array.Length-1; i++)
            {
                int j;
                int buf = array[i];
                for (j = i + 1; j >= 0; j--)
                {
                    if (array[j] < buf)
                        break;

                    array[j + 1] = array[j];
                }
                array[j + 1] = buf;
            }
        }

        static void Main(string[] args)
        {
        }
    }
}
