﻿using System;

namespace SelectionSort
{
    class Program
    {
        public static void SelectionSort(int[] arrayForSorting)
        {
            int min, temp;
            int length = arrayForSorting.Length;

            for (int i = 1; i < length - 1; i++)
            {
                min = i;

                for (int j = i + 1; j < length; j++)
                {
                    if (arrayForSorting[j] < arrayForSorting[min])
                    {
                        min = j;
                    }
                }

                if (min != i)
                {
                    temp = arrayForSorting[i];
                    arrayForSorting[i] = arrayForSorting[min];
                    arrayForSorting[min] = temp;
                }
            }
        }

        static void Main(string[] args)
        {
            int[] arrayForSorting = new int[] { 2, 3, 5, 6, 56, 45, 34, 78, 89, 90, 45, 34 };
            SelectionSort(arrayForSorting);
            Console.ReadKey();
        }
    }
}
